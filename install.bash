#!/bin/bash
# Hello this is an arch linux install script.
# all of this falls under the GPLv3 license
echo "###################################################"
echo "#Hello we are going to install Arch linux together#"
echo "###################################################"
sleep 5
# Testing network connection ( usually have network connection over ethernet but to be sure.)
ping -c 5 www.google.com
# This is all automated for /dev/sda, if you have hda then you can change that below.
sgdisk --zap-all /dev/sda
# This is still a bit of a mess but the layout is this boot(sda1),swap(sda2),root(sda3)
echo "n
p
1

+128M
a
1
n
p
2

+2G
t
2
82
n
p
3


w" | fdisk /dev/sda
#formatting to the right filesystem
mkfs.ext2 /dev/sda1
mkfs.ext4 /dev/sda3
#adding swap
mkswap /dev/sda2
swapon /dev/sda2
#mounting
mount /dev/sda3 /mnt
mkdir -p /mnt/boot
mount /dev/sda1 /mnt/boot

#Installing arch linux
touch /etc/pacman.d/mirrorlist
echo -e 'Server = http://arch.apt-get.eu/$repo/os/$arch
Server = http://mirror.i3d.net/pub/archlinux/$repo/os/$arch
Server = http://mirror.nl.leaseweb.net/archlinux/$repo/os/$arch' > /etc/pacman.d/mirrorlist
echo "


y" | pacstrap -i /mnt base base-devel
genfstab -U -p /mnt >> /mnt/etc/fstab
touch /mnt/etc/resolv.conf
echo -e 'nameserver 8.8.8.8
nameserver 8.8.4.4' > /mnt/etc/resolv.conf
#chrooting
touch /mnt/scriptv2.bash

#if you want a DE you can add that below "pacman -S i3 dmenu xorg xorg-xinit"
#but i'm trying to keep it as headless as possible
echo 'cat /dev/null > /etc/locale.gen
echo -e "en_US.UTF-8 UTF-8" > /etc/locale.gen
locale-gen
export LANG=en_US.UTF-8
echo localhost > /etc/hostname
pacman -Syy
sleep 2
yes | pacman -S grub efibootmgr os-prober dhcpcd
sleep 1
grub-install --recheck /dev/sda
grub-mkconfig -o /boot/grub/grub.cfg
systemctl enable dhcpcd.service
echo 'we setting password for root now'
sleep 2
passwd' > /mnt/scriptv2.bash
chmod +x /mnt/scriptv2.bash
arch-chroot /mnt ./scriptv2.bash
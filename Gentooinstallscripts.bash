#!/bin/bash

# This is a server gentoo install script
# it includes disk encryption on LVM
# requires internet connection + kernel knowledge if you want another kernel then my system 
# E3-1220v2 - 2x 120gb ssd - 32gb ram - 20TB HDDs - Intel server board ( S1200BTLR )
#
# However this is a automated installation of gentoo please check /etc/fstab and modify grub to your needs
# Also you might need to update the url var link for the hardened stage 3 to todays newest build.
# This all falls under the GPL v3 license

# some vars 
RED="\e[31m"
NONE="\e[39m"
# might wanna change this to the newest stage3 build
#
URL="http://mirror.leaseweb.com/gentoo/releases/amd64/autobuilds/current-stage3-amd64-hardened/stage3-amd64-hardened-20160303.tar.bz2"
FSYS="ext4"  # use any file system you like
SDA="/dev/sda" # change if you have another drive


echo -e "${RED}If you are unsure what you are doing, THIS WILL DELETE ALL DATA on the HDD${NONE}"
sleep 5
sgdisk --zap-all ${SDA}


# dev/sda1 = boot | /dev/sda2 = the rest
echo "n
p
1

+128M
a
n
p
2


w" | fdisk ${SDA}

mke2fs /dev/sda1

echo -e "${RED}You need to set your encryption password here${NONE}"
cryptsetup -v --cipher aes-xts-plain64 --key-size 512 --hash sha512 --iter-time 2000 --verify-passphrase luksFormat /dev/sda2

cryptsetup luksOpen /dev/sda2 root

pvcreate /dev/mapper/root

vgcreate vg /dev/mapper/root

lvcreate --size 2G --name swap vg
lvcreate --size 30G --name root vg
lvcreate --extents 100%FREE --name home vg

vgchange --available y

mkswap /dev/mapper/vg-swap
mkfs.${FSYS} /dev/mapper/vg-root
mkfs.${FSYS} /dev/mapper/vg-home
swapon /dev/mapper/vg-swap

mount /dev/mapper/vg-root /mnt/gentoo
mkdir -v /mnt/gentoo/{boot,home}
mount /dev/sda1 /mnt/gentoo/boot
mount /dev/mapper/vg-home /mnt/gentoo/home
cd /mnt/gentoo
wget ${URL}
tar -xjpf stage3*.tar.bz2
cd /
cp -L /etc/resolv.conf /mnt/gentoo/etc/
mount -t proc proc /mnt/gentoo/proc
mount --rbind /sys /mnt/gentoo/sys
mount --rbind /dev /mnt/gentoo/dev





echo '
source /etc/profile
cp /usr/share/zoneinfo/Europe/Amsterdam /etc/localtime
echo -e "Europe/Amsterdam" > /etc/timezone
date
sleep 2
cd /etc
echo -e "127.0.0.1 dickie.dickie.com dickie localhost" > hosts
sed -i -e 's/hostname.*/hostname="dickie"/' conf.d/hostname
hostname dickie
emerge-webrsync
echo "
CFLAGS="\"-march=native -O2 -pipe\"" 
CXXFLAGS="\"${CFLAGS}\""
CPU_FLAGS_X86="\"mmx mmxext sse sse2 sse3 ssse3\""
CHOST="\"x86_64-pc-linux-gnu\""
USE="\"cryptsetup bindist mmx sse2 aes sse3 sssse3\""
VIDEO_CARDS="\"intel\""
MAKEOPTS="\"-j2\""
INPUT_DEVICES="\"evdev\""
PORTDIR="\"/usr/portage\""
DISTDIR="\"${PORTDIR}/distfiles\""
PKGDIR="\"${PORTDIR}/packages\""
" > /etc/portage/make.conf
emerge -av hardened-sources genkernel
genkernel --menuconfig --install --symlink --lvm --luks all
emerge --ask --newuse grub sys-boot/os-prober
grub2-install /dev/sda
echo "
GRUB_CMDLINE_LINUX="\"dolvm root=/dev/mapper/vg-root rootfstype=ext4 crypt_root=/dev/sda2\""" >> /etc/default/grub
grub2-mkconfig -o /boot/grub/grub.cfg
emerge -av dhcpcd syslog-ng vixie-cron ntp superadduser sys-kernel/linux-firmware app-admin/syslog-ng
rc-update add dhcpcd default
rc-update add syslog-ng default
rc-update add vixie-cron default
rc-update add ntp-client default
rc-update add syslog-ng default
passwd
superadduser kaks
cd /
rm -rf stage3-*.tar.bz2*
'> /mnt/gentoo/scriptv2.bash
chmod +x /mnt/gentoo/scriptv2.bash
chroot /mnt/gentoo ./scriptv2.bash
#!/bin/bash


# Simple web server iptables script
# usage chmod +x iptables.bash
# ./iptables

IP="/sbin/iptables"

$IP -F

$IP -P INPUT DROP
$IP -P FORWARD DROP
$IP -P OUTPUT ACCEPT # might wanna consider DROP, requires other rules


$IP -A INPUT -m conntrack --ctstate ESTABLISHED,RELATED -j ACCEPT
$IP -A INPUT -i lo -j ACCEPT # accept local
$IP -A INPUT -p tcp --dport 22 -j ACCEPT # SSH 
$IP -A INPUT -p tcp --dport http -j ACCEPT # HTTP
$IP -A INPUT -p tcp --dport https -j ACCEPT # HTTPS



# if DROP is used as OUTPUT
# $IP -A OUTPUT -o eth0 -m state --state NEW,ESTABLISHED,RELATED -j ACCEPT
# $IP -A OUTPUT -p udp --sport 53 -m state --state ESTABLISHED,RELATED -j ACCEPT
# $IP -A OUTPUT -p tcp --sport 53 -m state --state ESTABLISHED,RELATED -j ACCEPT